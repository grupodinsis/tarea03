package com.silvanacorrea.Tarea03

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_destino.*

class destinoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_destino)

        //Obtenemos el bundle
        val bundle : Bundle? = intent.extras

        //let
        bundle?.let { bundleLibriDeNull ->

            val nombreDestino =  bundleLibriDeNull.getString("key_nombre","Desconocido")
            val edadDestino = bundleLibriDeNull.getString("key_edad","0")
            val mascota = bundleLibriDeNull.getString("key_mascota","")
            val vacuna01 = bundleLibriDeNull.getString("key_vacuna01","")
            val vacuna02 = bundleLibriDeNull.getString("key_vacuna02","")
            val vacuna03 = bundleLibriDeNull.getString("key_vacuna03","")
            val vacuna04 = bundleLibriDeNull.getString("key_vacuna04","")
            val vacuna05 = bundleLibriDeNull.getString("key_vacuna05","")

            tvNombre.text = "Nombre: $nombreDestino"
            tvEdad.text ="Edad: $edadDestino"
            if(mascota == "Perro") ivPetSelected.setImageResource(R.drawable.perro)
            else if(mascota == "Gato") ivPetSelected.setImageResource(R.drawable.gato)
            else ivPetSelected.setImageResource(R.drawable.conejo)
            tvVacuna1.text = "$vacuna01"
            tvVacuna2.text = "$vacuna02"
            tvVacuna3.text = "$vacuna03"
            tvVacuna4.text = "$vacuna04"
            tvVacuna5.text = "$vacuna05"

        }
    }
}
package com.silvanacorrea.Tarea03

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnEnviar.setOnClickListener {

            val nombre = edtNombre.text.toString()
            val edad = edtEdad.text.toString()
            if(nombre.isEmpty()) {
                toats("Debe ingresar un nombre")
                return@setOnClickListener
            }

            if (edad.isEmpty()){
                toats("Debe ingresar edad")
                return@setOnClickListener
            }
            val mascota =  if (rbPerro.isChecked) "Perro"
                else if(rbGato.isChecked)"Gato"
                else "Conejo"


            val vacuna01 = if(chkParvovirus.isChecked) "Parvovirus: Si" else "Parvovirus: No"
            val vacuna02 = if(chkCoronavirus.isChecked)  "Coronavirus: Si" else "Coronavirus: No"
            val vacuna03 = if(chkLeptospira.isChecked) "Leptospira: Si" else "Leptospira: No"
            val vacuna04 = if(chkPanleucopenia.isChecked)  "Panleucopenia: Si" else "Panleucopenia: No"
            val vacuna05 = if(chkRabia.isChecked)  "Rabia: Si" else "Rabia: No"

            val bundle = Bundle().apply {
                putString("key_nombre",nombre)
                putString("key_edad",edad)
                putString("key_mascota",mascota)
                putString("key_vacuna01",vacuna01)
                putString("key_vacuna02",vacuna02)
                putString("key_vacuna03",vacuna03)
                putString("key_vacuna04",vacuna04)
                putString("key_vacuna05",vacuna05)


            }


            val intent = Intent(this,destinoActivity::class.java).apply{
                putExtras(bundle)
            }
            startActivity(intent)
        }
    }
    fun toats(mensaje:String)= Toast.makeText(this,"$mensaje", Toast.LENGTH_SHORT).show()
}